import 'dotenv/config';
import 'reflect-metadata';
import fs from 'fs';
import { createServer } from 'http';
import path from 'path';

import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import { Server as SocketIoServer } from 'socket.io';

import './utils/response/customSuccess';

import { logger } from 'logging';
import { VumosManager, VumosMessageType } from 'vumos/VumosManager';

import { errorHandler } from './middleware/errorHandler';
import { getLanguage } from './middleware/getLanguage';
import routes from './routes';
import { dbCreateConnection } from './typeorm/dbCreateConnection';

export const app = express();
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(getLanguage);
app.use(cors());

try {
  const accessLogStream = fs.createWriteStream(path.join(__dirname, '../log/access.log'), {
    flags: 'a',
  });
  app.use(morgan('combined', { stream: accessLogStream }));
} catch (err) {
  logger.error('Error loading express morgan logger', err);
}
app.use(morgan('combined'));

app.use('/', routes);

app.use(errorHandler);

// Add Socket.io server
const server = createServer(app);
const socketServer = new SocketIoServer(server);
socketServer.on('connection', (socket: any) => {
  console.log(`Testing ${socket}`);
});

// Vumos Manager
export const vumos = new VumosManager({
  name: 'Vumos Service Interface',
  description: 'An interface for easy Vumos service management',
});

// Initialize DB and manager
(async () => {
  await dbCreateConnection();
  try {
    const conn = await vumos.connect(process.env.NATS_URI || undefined);
    logger.info(`Nats client connected to server ${conn.info.server_name} [${conn.info.server_id}]`);

    vumos.sendHelloMessage();
  } catch (reason) {
    logger.error('Nats client failed to start!', reason);
  }
})();

// Start server
const port = process.env.PORT || 4000;
server.listen(port, () => {
  logger.info(`Server running on port ${port}`);
});
