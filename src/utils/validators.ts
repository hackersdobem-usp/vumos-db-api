export const filterKeys = (obj: { [key: string]: any }, keys: string[]) => {
  const ret = {};

  keys.forEach((key) => {
    if (obj[key] !== undefined) {
      ret[key] = obj[key];
    }
  });

  return ret;
};
