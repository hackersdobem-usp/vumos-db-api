export class BasicHTTPError extends Error {
  error_code: string;
  http_code: number;
  details?: any;

  constructor(http_code: number, message: string, details?: any) {
    super(message);
    this.http_code = http_code;
    this.details = details;
  }

  get HttpStatusCode() {
    return this.http_code;
  }

  get JSON() {
    return {
      status_code: this.http_code,
      message: this.message,
      details: this.details,
    };
  }
}
