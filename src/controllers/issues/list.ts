import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { CustomError } from 'utils/response/CustomError';

export const list = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Issue>('Issue');

  // Pagination Information
  const pagestr = req.query.page as string;
  const rowsstr = req.query.rows as string;
  const sortstr = req.query.sort as string;

  const page = parseInt(pagestr);
  const rows = parseInt(rowsstr);
  const sort: { [key: string]: 'ASC' | 'DESC' } = {};

  if (sortstr)
    sortstr.split(',').forEach((v) => {
      if (v.match(/^[a-z_]+:(asc|desc)$/)) {
        const [field, order] = v.split(':');

        sort[field] = order.toUpperCase() as 'ASC' | 'DESC';
      }
    });

  if (isNaN(page) || isNaN(rows)) {
    return next(new CustomError(400, 'Raw', 'listing requires integer parameters "page" and "rows"'));
  }

  try {
    const [issues, total] = await repo.findAndCount({
      // eslint-disable-next-line prettier/prettier
      select: [
        'id',
        'name',
        'description',
        'cve',
        'cvss',
        'extra',
        'notes',
        'last_verified_at',
        'notified_at',
        'resolved_at',
      ],
      order: sort,
      relations: ['service', 'service.target'],
      skip: page * rows,
      take: rows,
    });

    res.customSuccess(200, 'List of issues', { page, rows, total, list: issues });
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't retrieve list of issues.`, null, err);
    return next(customError);
  }
};
