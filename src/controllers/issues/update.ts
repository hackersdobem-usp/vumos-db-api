import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { CustomError } from 'utils/response/CustomError';

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Issue>('Issue');

  try {
    const issueUpdate = await repo.update(req.params.id, req.body);

    res.customSuccess(201, 'Issue updated', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update issue`, null, err);

    return next(customError);
  }
};
