import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { CustomError } from 'utils/response/CustomError';

export const create = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Issue>('Issue');

  try {
    let issue = null;

    if (!req.body.id) {
      issue = repo.create(req.body);
    } else {
      const updateData = { ...req.body };
      delete updateData.id;
      await repo.update(req.body.id, updateData);
      issue = await repo.findOneOrFail(req.body.id);
    }

    await repo.save(issue);

    res.customSuccess(201, 'Issue created', issue);
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't create issue`, null, err);

    return next(customError);
  }
};
