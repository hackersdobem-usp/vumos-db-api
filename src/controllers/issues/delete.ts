import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { CustomError } from 'utils/response/CustomError';

export const c_delete = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Issue>('Issue');

  try {
    const issueUpdate = await repo.softDelete(req.params.id);

    res.customSuccess(204, 'Issue deleted', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update issue`, null, err);

    return next(customError);
  }
};
