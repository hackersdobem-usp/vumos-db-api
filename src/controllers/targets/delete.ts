import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { CustomError } from 'utils/response/CustomError';

export const c_delete = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Target>('Target');

  try {
    const targetUpdate = await repo.softDelete(req.params.id);

    res.customSuccess(204, 'Target deleted', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update target`, null, err);

    return next(customError);
  }
};
