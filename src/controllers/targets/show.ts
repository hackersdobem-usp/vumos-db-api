import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { CustomError } from 'utils/response/CustomError';

export const show = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Target>('Target');
  try {
    const targets = await repo.findOneOrFail({
      select: ['id', 'ip_address', 'domains', 'extra', 'notes', 'created_at', 'updated_at'],
      relations: ['services'],
    });

    res.customSuccess(200, 'Target value', targets);
  } catch (err) {
    console.log(err);
    const customError = new CustomError(400, 'Raw', `Can't retrieve user.`, null, err);
    return next(customError);
  }
};
