import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { CustomError } from 'utils/response/CustomError';

export const list = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Target>('Target');

  // Pagination Information
  const pagestr = req.query.page;
  const rowsstr = req.query.rows;
  const sortstr = req.query.sort as string;

  const page = parseInt(pagestr as string);
  const rows = parseInt(rowsstr as string);
  const sort: { [key: string]: 'ASC' | 'DESC' } = {};

  if (sortstr)
    sortstr.split(',').forEach((v) => {
      if (v.match(/^[a-z_]+:(asc|desc)$/)) {
        const [field, order] = v.split(':');

        sort[field] = order.toUpperCase() as 'ASC' | 'DESC';
      }
    });

  if (isNaN(page) || isNaN(rows)) {
    return next(new CustomError(400, 'Raw', 'listing requires integer parameters "page" and "rows"'));
  }

  try {
    const [targets, total] = await repo.findAndCount({
      select: ['id', 'ip_address', 'domains', 'extra', 'notes', 'created_at', 'updated_at'],
      order: sort,
      relations: ['services'],
      skip: page * rows,
      take: rows,
    });

    res.customSuccess(200, 'List of targets', { page, rows, total, list: targets });
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't retrieve list of targets.`, null, err);
    return next(customError);
  }
};
