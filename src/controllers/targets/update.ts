import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { CustomError } from 'utils/response/CustomError';

type RequestData = {
  domains?: string[];
  extra?: any;
  notes?: string;
};

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Target>('Target');

  try {
    const targetUpdate = await repo.update(req.params.id, req.body);

    res.customSuccess(201, 'Target updated', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update target`, null, err);

    return next(customError);
  }
};
