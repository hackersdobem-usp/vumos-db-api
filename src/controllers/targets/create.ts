import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { CustomError } from 'utils/response/CustomError';

export const create = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Target>('Target');

  try {
    let target = null;

    if (!req.body.id) {
      target = repo.create(req.body);
    } else {
      const updateData = { ...req.body };
      delete updateData.id;
      await repo.update(req.body.id, updateData);
      target = await repo.findOneOrFail(req.body.id);
    }

    await repo.save(target);

    res.customSuccess(201, 'Target created', target);
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't create target`, null, err);

    return next(customError);
  }
};
