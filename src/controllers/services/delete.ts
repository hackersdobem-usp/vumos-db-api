import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Service } from 'typeorm/entities/services/Service.entity';
import { CustomError } from 'utils/response/CustomError';

export const c_delete = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Service>('Service');

  try {
    const serviceUpdate = await repo.softDelete(req.params.id);

    res.customSuccess(204, 'Service deleted', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update service`, null, err);

    return next(customError);
  }
};
