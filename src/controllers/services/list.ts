import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Service } from 'typeorm/entities/services/Service.entity';
import { CustomError } from 'utils/response/CustomError';

export const list = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Service>('Service');

  // Pagination Information
  const pagestr = req.query.page as string;
  const rowsstr = req.query.rows as string;
  const sortstr = req.query.sort as string;

  const page = parseInt(pagestr);
  const rows = parseInt(rowsstr);
  const sort: { [key: string]: 'ASC' | 'DESC' } = {};

  if (sortstr)
    sortstr.split(',').forEach((v) => {
      if (v.match(/^[a-z_]+:(asc|desc)$/)) {
        const [field, order] = v.split(':');

        sort[field] = order.toUpperCase() as 'ASC' | 'DESC';
      }
    });

  if (isNaN(page) || isNaN(rows)) {
    return next(new CustomError(400, 'Raw', 'listing requires integer parameters "page" and "rows"'));
  }

  try {
    const [services, total] = await repo.findAndCount({
      select: ['id', 'name', 'port', 'protocol', 'version', 'extra', 'created_at', 'updated_at'],
      order: sort,
      relations: ['target'],
      skip: page * rows,
      take: rows,
    });

    res.customSuccess(200, 'List of services', { page, rows, total, list: services });
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't retrieve list of services.`, null, err);
    return next(customError);
  }
};
