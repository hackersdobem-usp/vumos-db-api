import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Service } from 'typeorm/entities/services/Service.entity';
import { CustomError } from 'utils/response/CustomError';

export const create = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Service>('Service');

  try {
    let service = null;

    if (!req.body.id) {
      service = repo.create(req.body);
    } else {
      const updateData = { ...req.body };
      delete updateData.id;
      await repo.update(req.body.id, updateData);
      service = await repo.findOneOrFail(req.body.id);
    }

    await repo.save(service);

    res.customSuccess(201, 'Service created', service);
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't create service`, null, err);

    return next(customError);
  }
};
