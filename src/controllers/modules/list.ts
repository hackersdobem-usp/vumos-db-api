import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Module } from 'typeorm/entities/modules/Module.entity';
import { BasicHTTPError } from 'utils/response/BasicHTTPError';
import { CustomError } from 'utils/response/CustomError';

export const list = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Module>('Module');

  // Pagination Information
  const pagestr = req.query.page;
  const rowsstr = req.query.rows;

  const page = parseInt(pagestr as string);
  const rows = parseInt(rowsstr as string);

  if (isNaN(page) || isNaN(rows)) {
    return next(new CustomError(400, 'Raw', 'listing requires integer parameters "page" and "rows"'));
  }

  try {
    const [modules, total] = await repo.findAndCount({
      select: [
        'id',
        'uid',
        'name',
        'configurations',
        'nickname',
        'description',
        'status_code',
        'status_message',
        'status_expiry',
        'status_last_update',
        'created_at',
        'updated_at',
      ],
    });

    res.customSuccess(200, 'List of modules', { page, rows, total, list: modules });
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't retrieve list of modules.`, null, err);
    return next(customError);
  }
};
