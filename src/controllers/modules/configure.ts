import Module from 'module';

import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { vumos } from 'index';
import { CustomError } from 'utils/response/CustomError';

export const configure = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Module>('Module');

  try {
    const module = await repo.findOne(req.params.id);

    if (!module) next(new CustomError(404, 'Raw', 'Module was not found'));

    vumos.updateConfiguration(req.params.id, req.body);

    res.customSuccess(200, 'Configuration Change Sent');
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update service`, null, err);

    return next(customError);
  }
};
