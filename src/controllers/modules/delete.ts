import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Module } from 'typeorm/entities/modules/Module.entity';
import { CustomError } from 'utils/response/CustomError';

export const c_delete = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Module>('Module');

  try {
    const moduleUpdate = await repo.softDelete(req.params.id);

    res.customSuccess(201, 'Module updated', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update module`, null, err);

    return next(customError);
  }
};
