import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Service } from 'typeorm/entities/services/Service.entity';
import { CustomError } from 'utils/response/CustomError';

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Service>('Service');

  try {
    const serviceUpdate = await repo.update(req.params.id, req.body);

    res.customSuccess(201, 'Service updated', await repo.findOne(req.params.id));
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't update service`, null, err);

    return next(customError);
  }
};
