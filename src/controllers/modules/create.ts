import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Module } from 'typeorm/entities/modules/Module.entity';
import { CustomError } from 'utils/response/CustomError';

export const create = async (req: Request, res: Response, next: NextFunction) => {
  const repo = getRepository<Module>('Module');

  try {
    let module = null;

    if (!req.body.id) {
      module = repo.create(req.body);
    } else {
      const updateData = { ...req.body };
      delete updateData.id;
      await repo.update(req.body.id, updateData);
      module = await repo.findOneOrFail(req.body.id);
    }

    await repo.save(module);

    res.customSuccess(201, 'Module created', module);
  } catch (err) {
    const customError = new CustomError(400, 'Raw', `Can't create module`, null, err);

    return next(customError);
  }
};
