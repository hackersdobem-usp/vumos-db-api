import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import validator from 'validator';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { BasicHTTPError } from 'utils/response/BasicHTTPError';
import { CustomError, ErrorValidation } from 'utils/response/CustomError';
import { filterKeys } from 'utils/validators';

export const validatorCreateTarget = async (req: Request, res: Response, next: NextFunction) => {
  const { id, ip_address, domains, extra, notes } = filterKeys(req.body, [
    'id',
    'ip_address',
    'domains',
    'extra',
    'notes',
  ]) as {
    id?: string;
    ip_address: string;
    domains: string[];
    extra?: any;
    notes?: string;
  };

  const errorsValidation: ErrorValidation[] = [];

  try {
    // Check id
    if (id && validator.isUUID(id || '')) {
      const target = await getRepository<Target>('Target').findOne(id);
      if (!target) throw new BasicHTTPError(404, `ID[${id}] was not found`);
    }

    // Check conflict
    if (await getRepository<Target>('Target').findOne({ where: { ip_address } })) {
      throw new BasicHTTPError(409, `Another target with given IP Address[${ip_address}] was found`);
    }

    if (!id && !validator.isIP(ip_address || '')) {
      // Validate fields
      // Check IPv4
      errorsValidation.push({ ip_address: 'IP Address[ip_address] is invalid' });
    }

    if (id && ip_address) {
      errorsValidation.push({ ip_address: 'IP Address[ip_address] cannot be updated' });
    }

    // Check domains
    if (domains) {
      const domainMatcher = /^(?!:\/\/)(?=.{1,255}$)((.{1,63}\.){1,127}(?![0-9]*$)[a-z0-9-]+\.?)$/gim;
      const invalidDomains = domains.filter((item) => !item.match(domainMatcher));
      if (invalidDomains.length > 0) {
        errorsValidation.push({ domains: `Domains[domains] ${invalidDomains.join(', ')} are invalid` });
      }
    } else {
      errorsValidation.push({ domains: 'Domains[domains] are required' });
    }
  } catch (err) {
    return next(err);
  }

  // Generate custom error array
  if (errorsValidation.length !== 0) {
    const customError = new CustomError(400, 'Validation', 'Validation error', null, null, errorsValidation);
    return next(customError);
  }

  return next();
};
