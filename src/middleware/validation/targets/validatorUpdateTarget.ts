import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Target } from 'typeorm/entities/targets/Target.entity';
import { BasicHTTPError } from 'utils/response/BasicHTTPError';
import { CustomError, ErrorValidation } from 'utils/response/CustomError';
import { filterKeys } from 'utils/validators';

export const validatorUpdateTarget = async (req: Request, res: Response, next: NextFunction) => {
  const { domains, extra, notes } = filterKeys(req.body, ['domains', 'extra', 'notes']) as {
    domains?: string[];
    extra?: any;
    notes?: string;
  };

  const errorsValidation: ErrorValidation[] = [];

  try {
    // Check id
    const target = await getRepository<Target>('Target').findOne(req.params.id);
    if (!target) throw new BasicHTTPError(404, `ID[${req.params.id}] was not found`);

    // Check domains
    if (domains) {
      const domainMatcher = /^(?!:\/\/)(?=.{1,255}$)((.{1,63}\.){1,127}(?![0-9]*$)[a-z0-9-]+\.?)$/gim;
      const invalidDomains = domains.filter((item) => !item.match(domainMatcher));
      if (invalidDomains.length > 0) {
        errorsValidation.push({ domains: `Domains ${invalidDomains.join(', ')} are invalid` });
      }
    } else {
      errorsValidation.push({ domains: 'Domains are required' });
    }
  } catch (err) {
    return next(err);
  }

  // Generate custom error array
  if (errorsValidation.length !== 0) {
    const customError = new CustomError(400, 'Validation', 'Validation error', null, null, errorsValidation);
    return next(customError);
  }

  return next();
};
