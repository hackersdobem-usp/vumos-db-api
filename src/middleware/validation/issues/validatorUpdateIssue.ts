import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { CustomError, ErrorValidation } from 'utils/response/CustomError';
import { filterKeys } from 'utils/validators';

export const validatorUpdateIssue = async (req: Request, res: Response, next: NextFunction) => {
  const errorsValidation: ErrorValidation[] = [];

  // Check id
  const issue = await getRepository<Issue>('Issue').findOne(req.params.id);
  if (!issue) errorsValidation.push({ id: 'ID[id] was not found' });

  // TODO: validate stuff

  // Generate custom error array
  if (errorsValidation.length !== 0) {
    const customError = new CustomError(400, 'Validation', 'Validation error', null, null, errorsValidation);
    return next(customError);
  }

  return next();
};
