import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { Service } from 'typeorm/entities/services/Service.entity';
import { CustomError, ErrorValidation } from 'utils/response/CustomError';
import { filterKeys } from 'utils/validators';

export const validatorUpdateService = async (req: Request, res: Response, next: NextFunction) => {
  const { domains, extra, notes } = filterKeys(req.body, ['domains', 'extra', 'notes']) as {
    domains?: string[];
    extra?: any;
    notes?: string;
  };

  const errorsValidation: ErrorValidation[] = [];

  // Check id
  const service = await getRepository<Service>('Service').findOne(req.params.id);
  if (!service) errorsValidation.push({ id: 'ID[id] was not found' });

  // Check domains
  if (domains) {
    const domainMatcher = /^(?!:\/\/)(?=.{1,255}$)((.{1,63}\.){1,127}(?![0-9]*$)[a-z0-9-]+\.?)$/gim;
    const invalidDomains = domains.filter((item) => !item.match(domainMatcher));
    if (invalidDomains.length > 0) {
      errorsValidation.push({ domains: `Domains ${invalidDomains.join(', ')} are invalid` });
    }
  } else {
    errorsValidation.push({ domains: 'Domains are required' });
  }

  // Generate custom error array
  if (errorsValidation.length !== 0) {
    const customError = new CustomError(400, 'Validation', 'Validation error', null, null, errorsValidation);
    return next(customError);
  }

  return next();
};
