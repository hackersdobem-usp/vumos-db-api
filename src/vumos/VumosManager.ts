import os from 'os';
import { TextEncoder, TextDecoder } from 'util';

import _ from 'lodash';
import { DateTime } from 'luxon';
import { connect, Msg, NatsConnection } from 'nats';
import { getConnection, getManager, getRepository } from 'typeorm';
import { v4 } from 'uuid';

import { logger } from 'logging';
import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { Module, ModuleConfiguration, ModuleConfigurationValue } from 'typeorm/entities/modules/Module.entity';
import { Service } from 'typeorm/entities/services/Service.entity';
import { Target } from 'typeorm/entities/targets/Target.entity';

export enum VumosMessageType {
  HELLO = 'hello',
  CCHANGE = 'configuration_change',
  CCHANGED = 'configuration_changed',
  SUPDATE = 'status_update',
  DTARGET = 'data_target',
  DSERVICE = 'data_service',
  DISSUE = 'data_issue',
}

export interface VumosMessageBase {
  id?: string;
  source?: string;
  mode?: 'broadcast' | 'targeted';
}

export type VumosHelloMessage = VumosMessageBase & {
  message: VumosMessageType.HELLO;

  data: {
    name: string;
    description: string;
    status_expiry?: number;
  };
};

export type VumosConfigurationChangeMessage = VumosMessageBase & {
  message: VumosMessageType.CCHANGE;

  data: {
    configurations: {
      key: string;
      value: {
        type: string;
        current: unknown;
      };
    }[];
  };
};

export type VumosConfigurationChangedMessage = VumosMessageBase & {
  message: VumosMessageType.CCHANGED;

  data: {
    configurations: ModuleConfiguration[];
  };
};

export type VumosStatusUpdateMessage = VumosMessageBase & {
  message: VumosMessageType.SUPDATE;

  data: {
    code: string;
    message: string;
  };
};

export type VumosTargetDataMessage = VumosMessageBase & {
  message: VumosMessageType.DTARGET;

  data: {
    ip_address: string;
    domains: string[];
    extra: any;
  };
};

export type VumosServiceDataMessage = VumosMessageBase & {
  message: VumosMessageType.DSERVICE;

  data: {
    ip_address: string;
    port: number;
    protocol: string;
    name: string;
    version: string;
    extra: any;
  };
};

export type VumosIssueDataMessage = VumosMessageBase & {
  message: VumosMessageType.DISSUE;

  data: {
    ip_address: string;
    port: number;
    name: string;
    description: string;
    cve: string;
    cvss: string;
    extra: any;
    notes: string;
    fixed: boolean;
  };
};

type VumosMessage =
  | VumosHelloMessage
  | VumosConfigurationChangeMessage
  | VumosConfigurationChangedMessage
  | VumosStatusUpdateMessage
  | VumosTargetDataMessage
  | VumosIssueDataMessage
  | VumosServiceDataMessage;

export class VumosManager {
  id: string;
  name: string;
  description: string;
  connection?: NatsConnection;

  constructor(props: { name: string; description: string }) {
    this.id = process.env.VUMOS_ID || v4();

    this.name = props.name;
    this.description = props.description;
  }

  /**
   * Connects to the nats server and sets up message subscriptions
   *
   * @param uri Nats URI string
   *
   * @returns Nats connection
   */
  async connect(uri: string | string[] = 'nats://nats') {
    const conn = await connect({
      servers: uri,
    });

    conn.subscribe('broadcast', {
      callback: (err, msg) => {
        const payload: VumosMessage = JSON.parse(new TextDecoder('utf-8').decode(msg.data));

        this.onMessage(msg, payload);
      },
    });

    conn.subscribe(this.id, {
      callback: (err, msg) => {
        const payload: VumosMessage = JSON.parse(new TextDecoder('utf-8').decode(msg.data));

        this.onMessage(msg, payload);
      },
    });

    this.connection = conn;

    return conn;
  }

  public async updateConfiguration(moduleId: string, updates: Record<string, unknown>) {
    const manager = await getManager();
    const moduleRepo = manager.getRepository<Module>('Module');
    const module = await moduleRepo.findOne(moduleId);

    const configurations = Object.keys(updates)
      .map((key) => {
        const config = module.configurations.find((config) => config.key === key);

        if (!config) return null;

        return { key, value: { type: config.value.type, current: updates[key] } };
      })
      .filter((config) => config);

    if (module) {
      const message: VumosMessage = {
        message: VumosMessageType.CCHANGE,
        data: {
          configurations,
        },
      };

      this.sendMessage(message, module.uid);
    }
  }

  /**
   * Sends a generic vumos message
   *
   * @param message Vumos message data (message and data keys filled)
   * @param to Targeted destination queue
   */
  public async sendMessage(message: VumosMessage, to?: string) {
    const msg = {
      id: this.id,
      source: 'manager',

      message: message.message,
      mode: to ? 'targeted' : 'broadcast',

      processed: [],

      data: { ...message.data },
    };

    console.log('========== Sending ==========');
    if (to) console.log(`destination: ${to}`);
    console.log(msg);

    this.connection.publish(to || 'broadcast', new TextEncoder().encode(JSON.stringify(msg)), {
      reply: this.id,
    });
  }

  /**
   * Sends a 'hello' vumos message
   *
   * @param to Targeted destination queue
   */
  public async sendHelloMessage(to?: string) {
    const hello: VumosHelloMessage = {
      message: VumosMessageType.HELLO,
      data: {
        name: this.name,
        description: this.description,
      },
    };

    this.sendMessage(hello, to);
  }

  /**
   * Callback for Nats Messages
   *
   * @argument message Raw NATS unprocessed message
   * @argument payload Vumos message payload (Parsed JSON)
   */
  private async onMessage(message: Msg, payload: VumosMessage) {
    await getManager().transaction(async (manager) => {
      const moduleRepo = manager.getRepository<Module>('Module');

      // Ignore own messages and other manager's messages
      if (payload.id === this.id || payload.source === 'manager') return;

      console.log('========== Receive ==========');
      console.log(payload);

      // Get module
      let module = await moduleRepo.findOne({ where: { uid: payload.id } });

      // If not a hello, and module does not exist, send hello, and ignore current message
      if (!module && payload.message !== VumosMessageType.HELLO) {
        this.sendHelloMessage();
        return;
      }

      // Process messages
      switch (payload.message) {
        /**
         * On HELLO message
         */
        case VumosMessageType.HELLO: {
          const data = payload.data;

          if (!module) {
            module = moduleRepo.create({
              uid: payload.id,
              ...data,
            });
          }

          module.name = data.name;
          module.description = data.description;
          module.status_expiry = data.status_expiry;

          await moduleRepo.save(module);

          if (payload.mode === 'broadcast') {
            logger.info(`Received HELLO from ${payload.id}`);

            // Send HELLO back if received broadcast
            this.sendHelloMessage(module.uid);
          }

          break;
        }
        /**
         * On CCHANGED message
         */
        case VumosMessageType.CCHANGED: {
          logger.info(`Received configuration changed from ${payload.id}`);

          // Find module
          const module = await moduleRepo.findOne({ where: { uid: payload.id } });

          module.configurations = payload.data.configurations;

          await moduleRepo.save(module);

          break;
        }
        /**
         * On SUPDATE message
         */
        case VumosMessageType.SUPDATE: {
          logger.info(`Received status update from ${payload.id}`);

          // If service exists update it, else we missed the hello, so we should ask for a hello
          module.status_code = payload.data.code;
          module.status_message = payload.data.message;
          module.status_last_update = DateTime.now().toJSDate();

          await moduleRepo.save(module);

          break;
        }
        /**
         * On DTARGET message
         */
        case VumosMessageType.DTARGET: {
          const targetRepo = manager.getRepository<Target>('Target');

          let target = await targetRepo.findOne({ where: { ip_address: payload.data.ip_address } });

          if (!target) {
            logger.info(`Registering new target [${payload.data.ip_address}]`);
            target = targetRepo.create({
              ip_address: payload.data.ip_address,
              domains: payload.data.domains || [],
              extra: payload.data.extra,
            });
          } else {
            // Merge
            payload.data.domains.forEach((domain) => {
              const found = target.domains.findIndex((d) => d === domain) !== -1;

              if (!found) {
                target.domains.push(domain);
              }
            });

            _.merge(target.extra, payload.data.extra);
          }

          await targetRepo.save(target);

          break;
        }
        /**
         * On DSERVICE message
         */
        case VumosMessageType.DSERVICE: {
          const targetRepo = manager.getRepository<Target>('Target');
          const serviceRepo = manager.getRepository<Service>('Service');

          // Create target if does not exist
          await manager
            .createQueryBuilder()
            .insert()
            .into(Target)
            .values({
              ip_address: payload.data.ip_address,
              domains: [],
            })
            .onConflict('DO NOTHING')
            .execute();

          const target = await targetRepo.findOne({ ip_address: payload.data.ip_address });

          // Create service if it does not exist
          let service = await serviceRepo.findOne({ where: { target: target.id, port: payload.data.port } });

          if (!service) {
            logger.info(`Registering new service [${payload.data.ip_address}:${payload.data.port}]`);
            service = serviceRepo.create({
              name: payload.data.name,
              port: payload.data.port,
              protocol: payload.data.protocol,
              version: payload.data.version,
              extra: payload.data.extra,
              target,
            });
          } else {
            service.name = payload.data.name;
            service.protocol = payload.data.protocol;
            service.version = payload.data.version;

            _.merge(target.extra, payload.data.extra);
          }

          service = await serviceRepo.save(service);

          break;
        }
        case VumosMessageType.DISSUE: {
          const targetRepo = manager.getRepository<Target>('Target');
          const serviceRepo = manager.getRepository<Service>('Service');
          const issueRepo = manager.getRepository<Issue>('Issue');

          // Create target if does not exist
          await manager
            .createQueryBuilder()
            .insert()
            .into(Target)
            .values({
              ip_address: payload.data.ip_address,
              domains: [],
            })
            .onConflict('DO NOTHING')
            .execute();

          const target = await targetRepo.findOne({ ip_address: payload.data.ip_address });

          // Create service if it does not exist
          await manager
            .createQueryBuilder()
            .insert()
            .into(Service)
            .values({
              port: payload.data.port,
              target: target,
            })
            .onConflict('DO NOTHING')
            .execute();
          const service = await serviceRepo.findOne({ where: { target: target.id, port: payload.data.port } });

          let issue = await issueRepo.findOne({ where: { service: service.id, name: payload.data.name } });
          if (!issue) {
            // eslint-disable-next-line prettier/prettier
            logger.info(
              `Registering new issue [ ${payload.data.name} on ${payload.data.ip_address}:${payload.data.port}]`,
            );
            issue = issueRepo.create({
              name: payload.data.name,
              service,
              description: payload.data.description,
              cve: payload.data.cve,
              cvss: payload.data.cvss,
              extra: payload.data.extra,
              notes: payload.data.notes,
              last_verified_at: new Date(),
              notified_at: new Date(),
            });
          } else {
            issue.description = payload.data.description;
            issue.cve = payload.data.cve;
            issue.cvss = payload.data.cvss;
            issue.extra = payload.data.extra;
            issue.notes = payload.data.notes;
            issue.last_verified_at = new Date();
            if (payload.data.fixed && !issue.resolved_at) {
              // This means the issue was resolved
              issue.resolved_at = new Date();
            }
            if (!payload.data.fixed && issue.resolved_at) {
              // This means the issue was unresolved!
              issue.resolved_at = null;
            }

            _.merge(target.extra, payload.data.extra);
          }

          issue = await issueRepo.save(issue);

          break;
        }
        default:
      }
    });
  }
}
