import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';

import { logger } from 'logging';

import { Role } from '../entities/users/types';
import { User } from '../entities/users/User.entity';

export class SeedUsers1590519635401 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const user = new User();
    const userRepository = getRepository<User>('User');

    user.username = 'seijihariki';
    user.name = 'Victor Seiji Hariki';
    user.email = 'victorseijih@gmail.com';
    user.password = '123456';
    user.hashPassword();
    user.role = 'ADMINISTRATOR' as Role;
    await userRepository.save(user);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    logger.info('Not implemented');
  }
}
