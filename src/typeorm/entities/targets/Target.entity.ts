import { IsIP } from 'class-validator';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

import { Service } from '../services/Service.entity';

@Entity('targets')
export class Target {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    comment: 'Target IP address',
    unique: true,
  })
  @IsIP(4)
  ip_address: string;

  @Column('text', {
    array: true,
    comment: 'Domains directed to this address',
    nullable: true,
  })
  domains: string[];

  @Column('json', {
    comment: 'Extra data from scanner',
    nullable: true,
  })
  extra?: any;

  @Column({
    comment: 'User notes for this target',
    nullable: true,
  })
  notes?: string;

  // Relationships
  @OneToMany(() => Service, (service) => service.target)
  services: Service[];

  // Auto timestamps
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  @DeleteDateColumn({ nullable: true })
  deleted_at?: Date;
}
