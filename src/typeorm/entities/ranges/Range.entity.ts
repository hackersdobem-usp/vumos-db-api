import { IsIP } from 'class-validator';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

import { Target } from '../targets/Target.entity';

@Entity('ranges')
export class Range {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  description?: string;

  @Column()
  @IsIP(4)
  start_ip: string;

  @Column()
  @IsIP(4)
  end_ip: string;

  @Column({
    comment: 'User notes for this ip range',
    nullable: true,
  })
  notes?: string;

  // Relationships
  @ManyToOne(() => Target, (target) => target.services)
  target: Target;

  // Auto timestamps
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn()
  updated_at: Date;

  @Column({ nullable: true })
  @DeleteDateColumn()
  deleted_at?: Date;
}
