import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

import { Issue } from '../issues/Issue.entity';
import { Target } from '../targets/Target.entity';

@Entity('services')
export class Service {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('int')
  port: number;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  protocol: string;

  @Column({ nullable: true })
  version: string;

  @Column('json', {
    comment: 'Extra data from scanner',
    nullable: true,
  })
  extra?: any;

  @Column({
    comment: 'User notes for this service',
    nullable: true,
  })
  notes?: string;

  // Relationships
  @OneToMany(() => Issue, (issue) => issue.service)
  issues: Issue[];

  @ManyToOne(() => Target, (target) => target.services)
  target: Target;

  // Auto timestamps
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn()
  updated_at: Date;

  @Column({ nullable: true })
  @DeleteDateColumn()
  deleted_at?: Date;
}
