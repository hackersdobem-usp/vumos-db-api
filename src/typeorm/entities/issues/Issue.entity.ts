import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

import { Service } from '../services/Service.entity';

@Entity('issues')
export class Issue {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    comment: 'Issue name',
  })
  name: string;

  @Column({
    comment: 'Issue basic description',
  })
  description: string;

  @Column({
    comment: 'CVE identifier of the issue',
    nullable: true,
  })
  cve?: string;

  @Column({
    comment: 'CVSS 3.0 vector',
    nullable: true,
  })
  cvss?: string;

  @Column('json', {
    comment: 'Extra data from scanner',
    nullable: true,
  })
  extra?: any;

  @Column({
    comment: 'User notes for this issue',
    nullable: true,
  })
  notes?: string;

  @Column({ nullable: true })
  last_verified_at?: Date;

  @Column({ nullable: true })
  notified_at?: Date;

  @Column({ nullable: true })
  resolved_at?: Date;

  // Relationships
  @ManyToOne(() => Service, (service) => service.issues)
  service: Service;

  // Auto timestamps
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn()
  updated_at: Date;

  @Column({ nullable: true })
  @DeleteDateColumn()
  deleted_at?: Date;
}
