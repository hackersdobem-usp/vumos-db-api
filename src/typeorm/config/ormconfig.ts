import { join } from 'path';

import { ConnectionOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

// Entities
import { Issue } from 'typeorm/entities/issues/Issue.entity';
import { Module } from 'typeorm/entities/modules/Module.entity';
import { Range } from 'typeorm/entities/ranges/Range.entity';
import { Service } from 'typeorm/entities/services/Service.entity';
import { Target } from 'typeorm/entities/targets/Target.entity';
import { User } from 'typeorm/entities/users/User.entity';

console.log(process.env);

const config: ConnectionOptions = {
  type: 'postgres',
  host: process.env.PG_HOST,
  port: Number(process.env.PG_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: process.env.NODE_ENV === 'dev',
  logging: false,
  entities: [Issue, Module, Range, Service, Target, User],
  migrations: [join(__dirname, '**', '*.migration.{ts,js}')],
  subscribers: [join(__dirname, '**', '*.subscriber.{ts,js}')],
  cli: {
    entitiesDir: 'src/typeorm/entities',
    migrationsDir: 'src/typeorm/migrations',
    subscribersDir: 'src/typeorm/subscriber',
  },
  namingStrategy: new SnakeNamingStrategy(),
};

export = config;
