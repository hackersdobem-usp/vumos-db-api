import { Router } from 'express';

import { create, c_delete, list, update } from 'controllers/issues';
import { validatorCreateIssue } from 'middleware/validation/issues/validatorCreateIssue';
import { validatorUpdateIssue } from 'middleware/validation/issues/validatorUpdateIssue';
import { uuidv4 } from 'utils/matchers';

const router = Router();

// Read
router.get('/', [], list);

// Write
router.post('/', [validatorCreateIssue], create);
router.put(`/:id(${uuidv4})`, [validatorUpdateIssue], update);

router.delete(`/:id(${uuidv4})`, [], c_delete);
export default router;
