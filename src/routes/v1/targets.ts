import { Router } from 'express';

import { list, show, create, c_delete, update } from 'controllers/targets';
import { checkJwt } from 'middleware/checkJwt';
import { validatorCreateTarget } from 'middleware/validation/targets/validatorCreateTarget';
import { validatorUpdateTarget } from 'middleware/validation/targets/validatorUpdateTarget';
import { uuidv4 } from 'utils/matchers';

const router = Router();

// Read
router.get('/', [checkJwt], list);
router.get(`/:id(${uuidv4})`, [checkJwt], show);

// Write
router.post('/', [checkJwt, validatorCreateTarget], create);
router.put(`/:id(${uuidv4})`, [checkJwt, validatorUpdateTarget], update);

router.delete(`/:id(${uuidv4})`, [checkJwt], c_delete);

export default router;
