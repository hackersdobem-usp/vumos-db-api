import { Router } from 'express';

import { configure, create, c_delete, list, update } from 'controllers/modules';
import { uuidv4 } from 'utils/matchers';

const router = Router();

// Read
router.get('/', [], list);

// Write
router.post('/', [], create);
router.put(`/:id(${uuidv4})`, [], update);
router.put(`/:id(${uuidv4})/configuration`, [], configure);

router.delete(`/:id(${uuidv4})`, [], c_delete);
export default router;
