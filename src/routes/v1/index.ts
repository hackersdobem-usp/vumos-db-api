import { Router } from 'express';

import auth from './auth';
import issues from './issues';
import modules from './modules';
import services from './services';
import targets from './targets';
import users from './users';

const router = Router();

router.use('/auth', auth);
router.use('/users?', users);
router.use('/targets?', targets);
router.use('/services?', services);
router.use('/modules?', modules);
router.use('/issues?', issues);

export default router;
