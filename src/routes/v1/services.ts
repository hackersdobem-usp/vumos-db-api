import { Router } from 'express';

import { create, c_delete, list, update } from 'controllers/services';
import { validatorCreateService } from 'middleware/validation/services/validatorCreateService';
import { validatorUpdateService } from 'middleware/validation/services/validatorUpdateService';
import { uuidv4 } from 'utils/matchers';

const router = Router();

// Read
router.get('/', [], list);

// Write
router.post('/', [validatorCreateService], create);
router.put(`/:id(${uuidv4})`, [validatorUpdateService], update);

router.delete(`/:id(${uuidv4})`, [], c_delete);
export default router;
